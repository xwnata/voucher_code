package voucher_code

import (
	"math/rand"
	"time"
)

// Code , main instance of voucher code
type Code struct {
	Length  int
	Charset string
}

// constants used
const (
	Numeric           string = "0123456789"
	UpperAlpha        string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	LowerAlpha        string = "abcdefghijklmnopqrstuvwxyz"
	UpperAlphaNumeric string = UpperAlpha + Numeric // this is the default charset
	LowerAlphaNumeric string = LowerAlpha + Numeric
)

// New code instance code instance ready to use.
// l = length
// t = charset
// example : New(10, Numeric) ; when generating code, it will generate 10 numbers in random
func New(l int, cs string) Code {
	var code Code
	if l <= 0 { // default code length
		l = 8
	}
	code.Length = l

	if len(cs) <= 0 { // default charset upper alpha numeric
		cs = UpperAlphaNumeric
	}
	code.Charset = cs

	return code
}

// Generate a voucher code
func (c *Code) Generate() string {
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, c.Length)
	for i := range b {
		b[i] = c.Charset[seededRand.Intn(len(c.Charset))]
	}
	return string(b)
}

// GenerateBatch generates multiple voucher code
func (c *Code) GenerateBatch(amount int) []string {
	var result []string
	for i := 0; i < amount; i++ {
		result = append(result, c.Generate())
	}

	return result
}
